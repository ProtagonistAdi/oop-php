<?php

require_once('animal.php');
require_once('buduk.php');
require_once('sungokong.php');

$sheep = new animal("shaun");
echo "Name : " . $sheep ->name . "<br>";
echo "Legs : " . $sheep ->legs . "<br>";
echo "coldblooded : " . $sheep ->coldblooded . "<br>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok ->name . "<br>";
echo "Legs : " . $kodok ->legs . "<br>";
echo "coldblooded : " . $kodok ->coldblooded . "<br>";
$kodok ->lompat(); echo "<br>"; 

$sungokong = new Ape("Kera sakti");
echo "Name : " . $sungokong ->name . "<br>";
echo "Legs : " . $sungokong ->legs . "<br>";
echo "coldblooded : " . $sungokong ->coldblooded . "<br>";
$sungokong -> teriak(); echo "<br>";

?>